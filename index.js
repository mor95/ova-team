/**
 * /**
 * @file Archivo que contiene el módulo ova-team
 * @namespace index
 * @module ova-team
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 */

const $ = require('jquery');
const _ = require('lodash');
const swal = require('sweetalert2');
/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {
    currentProps: {
        enabled: true,
    },
    validateRenderArgs: function(args){
        
        if(typeof args.templateName === 'undefined'){
            throw new Error("A template name is expected.");
        }
        return true;
    },
    validateAppendArgs: function(args){
        if(!args.$target){
            throw new Error("A target is expected.");
        }
        if(!args.$target instanceof jQuery){
            throw new Error("Invalid target. A jQuery element was expected.");
        }
        if(!args.$target.length){
            throw new Error("jQuery target not available in DOM.");
        }
    },
    init: function(){
        
    },
    render: function(args){
        module.exports.validateRenderArgs(args);
        const template = require('templates/' + args.templateName + '.hbs');
        return $(template(args.locals));
    },
    renderTeamTree: function(args){
        module.exports.validateAppendArgs(args);
        if(!_.isObject(args.team)){
            throw new Error('A team is expected.')
        }
        _.forEach(args.team.groups, function(v, k){
            const $teamGroup = module.exports.render({
                $target: args.$target,
                locals: {
                    group: v.group
                },
                templateName: 'teamGroup'
            })
            .appendTo(args.$target);
            
            _.forEach(v.members, function(v, k){
                module.exports.render({
                    $target: $teamGroup,
                    locals: v,
                    templateName: 'teamMember'
                })
                .appendTo($teamGroup);
            });
        });
    },
    initializeTeamIcon: function(args){
        module.exports.validateAppendArgs(args);
        return module.exports.currentProps.$teamIcon = module.exports.render({
                    templateName: 'teamIcon'
                })
                .appendTo(args.$target)
                .on('click',  function(){
                    module.exports.currentProps.$teamModal.toggleClass('concealed');
                });
    },
    initializeTeamModal: function(args){
        module.exports.validateAppendArgs(args);
        return module.exports.currentProps.$teamModal = module.exports.render({
            templateName: 'teamModal'
        })
        .appendTo(args.$target)
        .find('.close-button').on('click',  function(){
            module.exports.currentProps.$teamModal.toggleClass('concealed');
        })
        .end();
    }
};